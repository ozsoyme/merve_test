package com.retouch.merve;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FolderController {
	
	@GetMapping( path="/folder/{id}")
	public Folder getFolder(@PathVariable int id){
	
		Folder f= new Folder();
		f.setId(id);
		f.setName("merveOzsoy");
		return f;
		
	}

}
